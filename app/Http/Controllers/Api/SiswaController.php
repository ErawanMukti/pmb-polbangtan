<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\Services\SiswaService;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index(Request $request, SiswaService $service)
    {
        $filter = $request->q == '' ? '%' : $request->q;
        return response()->json($service->paginateSiswa($filter));
    }

    public function getBiodataByNis(Request $request)
    {
        try {
            $endpoint = 'http://simdik.bppsdmp.pertanian.go.id/api';
            $client = new \GuzzleHttp\Client();
    
            $response = $client->request('GET', $endpoint, [
                'query' => [
                    'key' => '973b2119fe1d68770086936e1214972d',
                    'nipd' => $request->nisn
                ]
            ]);
    
            $statusCode = $response->getStatusCode();
            $content = json_decode($response->getBody()->getContents(), true);
    
            if ($statusCode == 200) {
                return $content;
            }    
        } catch(ClientException $e) {
        }
        return [];
    }
}
