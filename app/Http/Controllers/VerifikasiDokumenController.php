<?php

namespace App\Http\Controllers;

use App\Jurusan;
use App\Libs\Services\PendaftaranDetailService;
use App\Libs\Services\PendaftaranService;
use App\Libs\Services\PrestasiService;
use App\Libs\Traits\InfoPendaftaran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;

class VerifikasiDokumenController extends Controller
{
    use InfoPendaftaran;

    private $url = 'verifikasi-dokumen';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->url . '.index', [
            'urlDataList' => route('api.verifikasi')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PendaftaranService $service)
    {
        DB::transaction(function () use ($request, $service) {
            $data = $request->only(['keterangan_verifikasi']);
            $pendaftaran = $service->getPendaftaranById($request->pendaftaran_id);

            if ($pendaftaran->jalur == 'umum' || $pendaftaran->jalur == 'kerjasama-pemda') {
                $this->updateState($request->pendaftaran_id, $request->state);                
            } else if ( $pendaftaran->jalur == 'tugas-belajar' ) {
                if ( $request->state == 'memverifikasi_dokumen' ) {
                    $data['state'] = 'tes_kesehatan';
                } else {
                    $this->updateState($request->pendaftaran_id, $request->state);
                }
            } else {
                if ( $request->jalur == 'memverifikasi_dokumen' ) {
                    if ( $pendaftaran->keterangan == '' ) {
                        $data['state'] = 'tes_kesehatan';
                    } else {
                        $this->updateState($request->pendaftaran_id, $request->state);
                    }
                } else {
                    $this->updateState($request->pendaftaran_id, $request->state);
                }
            }

            $data['tanggal_verifikasi'] = Carbon::now();
            $service->updatePendaftaran($request->pendaftaran_id, $data);
        });

        return Redirect::to($this->url)->withSuccess('Verifikasi pendaftaran berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,
                         PendaftaranService $service,
                         PendaftaranDetailService $dService,
                         PrestasiService $pService)
    {
        $pendaftaran = $service->getPendaftaranById($id);
        $pendaftaran['jalur_label'] = ucwords(str_replace("-", " ", $pendaftaran->jalur));
        $pendaftaran['jurusan_1_label'] = Jurusan::selectName($pendaftaran->jurusan_1);
        $pendaftaran['jurusan_2_label'] = Jurusan::selectName($pendaftaran->jurusan_2);

        $detail = $dService->getPendaftaranDetailByPendaftaran($pendaftaran->id);
        $biodata = $detail->map(function($item) {
            return $item;
        })->where('kelompok', 'biodata');

        $dokumen = $detail->map(function($item) {
            return $item;
        })->where('kelompok', 'berkas');

        $cek_sistem_false = $detail->map(function($item) {
            return $item;
        })->where('cek_sistem', false)->count();

        $prestasi = $pService->getPrestasiByPendaftaran($pendaftaran->id);

        return view($this->url . '.show', [
            'pendaftaran' => $pendaftaran,
            'biodata' => $biodata,
            'dokumen' => $dokumen,
            'cek_sistem_false' => $cek_sistem_false,
            'prestasi' => $prestasi
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
