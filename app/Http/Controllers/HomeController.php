<?php

namespace App\Http\Controllers;

use App\Libs\Services\DashboardService;
use App\Libs\Services\InstitusiService;
use App\Libs\Services\JadwalService;
use App\Libs\Services\UserService;
use App\Libs\Traits\InfoOperator;
use App\Libs\Traits\InfoSiswa;
use App\Libs\Traits\InstitusiJurusan;
use Illuminate\Http\Request;
use Redirect;

class HomeController extends Controller
{
	use InfoOperator, InfoSiswa, InstitusiJurusan;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, JadwalService $service,
                          DashboardService $dService, InstitusiService $iService)
    {
    	$data = $request->user()->person;
    	$kelengkapan = true;
    	$state = '';
    	$pendaftaran = '';
    	$institusi = '';
        $jadwal = $service->getJadwal();
        
        $dash_tot_pendaftaran = 0;
        $dash_tot_lolos = 0;
        $dash_tot_gugur = 0;

        $dash_verdok_institusi = [];
        $dash_verdok_jurusan = [];

    	if ( $request->user()->person_type == 'siswa' ) {
    		$kelengkapan = $this->cekKelengkapanDokumen();
    		$state = $this->getPendaftaranState();
            $pendaftaran = $this->getPendaftaran();
        }
        
        if ( $request->user()->person_type == 'admin' ) {
            $dash_tot_pendaftaran = $dService->jmlPendaftaranByState('all');
            $dash_tot_gugur = $dService->jmlPendaftaranByState('gugur_dokumen');
            $dash_tot_lolos = $dService->jmlPendaftaranByState('lolos_dokumen');

            $institusi = $iService->getOnlyCabang();
            $dash_verdok_institusi = [];
            foreach($institusi as $key => $value) {
                $jumlah = $dService->verdokByInstitusi($value->id);
                array_push($dash_verdok_institusi, [
                    'nama' => $value->nama,
                    'lolos' => $jumlah['lolos'],
                    'gugur' => $jumlah['gugur'],
                ]);
            }
        }

        if ( $request->user()->person_type == 'operator' ) {
            $institusi = $this->getInstitusi();
            $jurusan = $this->getJurusan($institusi->id);

            $dash_verdok_institusi = $dService->verdokByInstitusi($institusi->id);
            $dash_verdok_jurusan = [];
            foreach($jurusan as $key => $value) {
                $jumlah = $dService->verdokByJurusan($value->id);
                array_push($dash_verdok_jurusan, [
                    'nama' => $value->nama,
                    'lolos' => $jumlah['lolos'],
                    'gugur' => $jumlah['gugur'],
                ]);
            }
        }

        return view('home', [
        	'data' => $data,
        	'kelengkapan' => $kelengkapan,
        	'state' => $state,
            'pendaftaran' => $pendaftaran,
            'institusi' => $institusi,
            'jadwal' => $jadwal,
            'dash_tot_pendaftaran' => $dash_tot_pendaftaran,
            'dash_tot_lolos' => $dash_tot_lolos,
            'dash_tot_gugur' => $dash_tot_gugur,
            'dash_verdok_institusi' => $dash_verdok_institusi,
            'dash_verdok_jurusan' => $dash_verdok_jurusan
        ]);
    }

    /**
     * [profile]
     * @param  Request $request
     */
    public function profile(Request $request)
    {
    	$data = $request->user();
    	return view('profile', ['data' => $data]);
    }

    /**
     * [update profile]
     * @param  Request $request
     * @param  int $id
     */
    public function update(Request $request, $id, UserService $service)
    {
        $request->validate([
            'password' => 'required|confirmed',
		]);

        $data = [];
        if ( $request->has('password') && !empty($request->password) ) {
            $data['password'] = bcrypt($request->password);
        }

        $service->updateUser($id, $data);
    	
        return Redirect::to('profile')->withSuccess('Berhasil merubah profile');
    }
}
