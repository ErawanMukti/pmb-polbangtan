<?php

namespace App\Http\Controllers;

use App\Libs\Services\InstitusiService;
use App\Libs\Services\PendaftaranService;
use App\Libs\Traits\InstitusiJurusan;
use Illuminate\Http\Request;

class VerifikasiAkhirController extends Controller
{
    use InstitusiJurusan;

    private $url = 'verifikasi-akhir';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PendaftaranService $service, InstitusiService $iService)
    {
        $pendaftaran = $service->getPendaftaranGroupByInstitusi(['state' => 'verifikasi_akhir']);
        $institusi = $iService->getOnlyCabang();
        $jurusan = $this->getJurusan($institusi[0]->id);

        return view($this->url . '.index', [
            'pendaftaran' => $pendaftaran,
            'institusi' => $institusi,
            'jurusan' => $jurusan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generate(Request $request, PendaftaranService $service)
    {
    	$pendaftaran = $service->searchPendaftaran([
    		'state' => 'verifikasi_akhir',
    		'jurusan_1' => $request->jurusan
    	]);
    	
        return view($this->url . '.generate', [
        	'pendaftaran' => $pendaftaran
        ]);
    }
}
