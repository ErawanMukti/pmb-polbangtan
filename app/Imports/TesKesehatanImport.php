<?php

namespace App\Imports;

use App\Libs\Traits\InfoPendaftaran;
use App\TesKesehatan;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class TesKesehatanImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable, InfoPendaftaran;

    protected $userId;

    public function __construct(int $user_id)
    {
        $this->userId = $user_id;
    }

    /**
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        //insert tes kesehatan
        $tesTulis = TesKesehatan::create([
            'no_pendaftaran' => $row['no_pendaftaran'],
            'mata' => $row['kesehatanmata'],
            'gigi' => $row['kondisigigi'],
            'tindik' => $row['tindik'],
            'cacat' => $row['cacatfisik'],
            'tato' => $row['tato'],
            'tinggi_badan' => $row['tinggibadan'],
            'penyakit' => $row['penyakitkronis'],
            'keterangan' => $row['keterangan'],
            'hasil' => strtolower($row['lulus_yt']),
            'uploaded_by' => $this->userId
        ]);

        //update state pendaftaran
        $pendaftaran = $tesTulis->pendaftaran;
        if ($row['lulus_yt'] == 'y') {
            $this->updateState($pendaftaran->id, 'memverifikasi_kesehatan');
        } else {
            $this->updateState($pendaftaran->id, 'mengugurkan_kesehatan');
        }

        //kirim email

        return $tesTulis;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'no_pendaftaran' => 'required|exists:pendaftarans|unique:tes_kesehatans',
            'kesehatanmata' => 'required',
            'kondisigigi' => 'required',
            'tindik' => 'required',
            'cacatfisik' => 'required',
            'tato' => 'required',
            'tinggibadan' => 'required|numeric',
            'penyakitkronis' => 'required',
            'lulus_yt' => Rule::in(['y', 't'])
        ];
    }
}
