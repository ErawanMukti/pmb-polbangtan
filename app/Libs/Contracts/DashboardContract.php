<?php

namespace App\Libs\Contracts;

interface DashboardContract
{
    public function jmlPendaftaranByState(String $state);
    
    public function verdokByInstitusi(int $institusiId);

    public function verdokByJurusan(int $jurusanId);
}