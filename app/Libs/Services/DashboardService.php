<?php

namespace App\Libs\Services;

use App\Pendaftaran;
use App\Libs\Contracts\DashboardContract;

class DashboardService implements DashboardContract
{
    private $model;
    private $stateGugurAdministrasi;
    private $statelulusAdministrasi;

    public function __construct(Pendaftaran $model)
    {
        $this->model = $model;
        $this->stateGugurAdministrasi = ['gugur_dokumen'];
        $this->statelulusAdministrasi = ['tes_tulis', 'tes_wawancara', 'tes_kesehatan', 'verifikasi_akhir', 'gugur_tulis',
        'gugur_wawancara', 'gugur_kesehatan', 'gugur_akhir', 'selesai'];
    }

    public function jmlPendaftaranByState(String $state)
    {
        if ($state == 'all') {
            return $this->model->count();
        }

        if ($state == 'gugur_dokumen') {
            return $this->model->where('state', $this->stateGugurAdministrasi)->count();
        }

        if ($state == 'lolos_dokumen') {
            return $this->model->whereIn('state', $this->statelulusAdministrasi)->count();
        }
    }

    public function verdokByInstitusi(int $institusiId)
    {
        $lolos = $this->model->whereInstitusi($institusiId)->whereIn('state', $this->statelulusAdministrasi)->count();
        $gugur = $this->model->whereInstitusi($institusiId)->where('state', $this->stateGugurAdministrasi)->count();

        return ['lolos' => $lolos, 'gugur' => $gugur];
    }

    public function verdokByJurusan(int $jurusanId)
    {
        $lolos_1 = $this->model->where('jurusan_1', $jurusanId)->whereIn('state', $this->statelulusAdministrasi)->count();
        $lolos_2 = 0; //$this->model->where('jurusan_2', $jurusanId)->whereIn('state', $this->statelulusAdministrasi)->count();

        $gugur_1 = $this->model->where('jurusan_1', $jurusanId)->where('state', $this->stateGugurAdministrasi)->count();
        $gugur_2 = 0; //$this->model->where('jurusan_2', $jurusanId)->where('state', $this->stateGugurAdministrasi)->count();

        return ['lolos' => $lolos_1 + $lolos_2, 'gugur' => $gugur_1 + $gugur_2];
    }
}
