@extends('layouts.gentellela')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-right">
					<button
					class="btn btn-app">
					<i class="fa fa-file-excel-o"></i> Export
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Daftar Pendaftaran</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="dashboard-widget-content">
							<div class="col-md-12 hidden-small">
								<table id="datatable-fixed-header" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>No Pendaftaran</th>
											<th>Nama Siswa</th>
                                            <th>Institusi</th>
                                            <th>Prodi 1</th>
                                            <th>Prodi 2</th>
                                            <th>State</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $key => $value)
										<tr>
											<td>{{ $value->no_pendaftaran }}</td>
                                            <td>{{ $value->siswa->nama }}</td>
											<td>{{ $value->minstitusi->nama }}</td>
                                            <td>{{ $value->jurusan_1_label }}</td>
                                            <td>{{ $value->jurusan_2_label }}</td>
                                            <td>{{ $value->state }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- /page content -->
@endsection

@section('js')
@if( Session::has('success') )
<script type="text/javascript">
	swal("Berhasil", "{{ Session::get('success') }}", "success");
</script>
@endif
@if( $errors->any() )
<script type="text/javascript">
	swal("Tidak dapat menyimpan data", "{{ Html::ul($errors->all()) }}", "error");
</script>
@endif

</script>
<script>
	$(document).ready(function(){
		$('#datatable-fixed-header').DataTable({
			fixedHeader: true
		});
        $.ajax({
            url: "{{ asset('js/helper.js') }}", dataType: "script",
        });

		var handleDataTableButtons = function() {
			if ($("#datatable-buttons").length) {
				$("#datatable-buttons").DataTable({
					dom: "Bfrtip",
					buttons: [
					{
						extend: "csv",
						className: "btn-sm"
					},
					{
						extend: "excel",
						className: "btn-sm"
					},
					{
						extend: "pdf",
						className: "btn-sm"
					},
					],
					responsive: true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init: function() {
					handleDataTableButtons();
				}
			};
		}();

		TableManageButtons.init();
	});

	$('#modal_update').on('show.bs.modal', function(e){
		$("#form_update").prop('action', $(e.relatedTarget).data('url'));
		$("#nama_update").val($(e.relatedTarget).data('nama'));
		$("#institusi_update").val($(e.relatedTarget).data('institusi'));
		$("#email_update").val($(e.relatedTarget).data('email'));
	});

</script>
<!-- /Doughnut Chart -->
@endsection