<table>
    <thead>
        <tr>
            <th>No</th>
            <th>No Pendaftaran</th>
            <th>Siswa</th>
            <th>Kesehatan<br/>Mata</th>
            <th>Kondisi<br/>Gigi</th>
            <th>Tindik</th>
            <th>Cacat<br/>Fisik</th>
            <th>Tato</th>
            <th>Tinggi<br/>Badan</th>
            <th>Penyakit<br/>Kronis</th>
            <th>Keterangan</th>
            <th>Lulus (Y/T)</th>
        </tr>
    </thead>
    <tbody>
    @php $no=1; @endphp
    @foreach($pendaftaran as $key => $value)
        <tr>
            <td>{{ $no }}</td>
            <td>{{ $value->no_pendaftaran }}</td>
            <td>{{ $value->siswa->nama }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @php $no++; @endphp
    @endforeach
    </tbody>
</table>