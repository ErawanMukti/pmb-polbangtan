<div class="row top_tiles">
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-edit"></i></div>
            <div class="count">{{ $dash_tot_pendaftaran }}</div>
            <h3>Daftar Peserta</h3>
            <p>Daftar seluruh peserta PMB</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-check-square-o"></i></div>
            <div class="count">{{ $dash_tot_gugur }}</div>
            <h3>Peserta Gugur</h3>
            <p>Daftar peserta gugur administrasi</p>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-sign-in"></i></div>
            <div class="count">{{ $dash_tot_lolos }}</div>
            <h3>Peserta Lolos</h3>
            <p>Daftar peserta lolos administrasi</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
        <div class="x_panel">
            <div class="x_title">
                <h2>Hasil Verifikasi Dokumen <small>Grafik</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <canvas id="canvas1i" style="margin: 5px 10px 10px 0"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="x_panel">
            <div class="x_title">
                <h2>Hasil Verifikasi Dokumen <small>Per Institusi</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Institusi</th>
                            <th>Lulus</th>
                            <th>Gugur</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no = 1; $tot_lolos = 0; $tot_gugur = 0; @endphp
                        @foreach($dash_verdok_institusi as $key => $value)
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $value['nama'] }}</td>
                                <td>{{ $value['lolos'] }}</td>
                                <td>{{ $value['gugur'] }}</td>
                                <td>{{ $value['lolos'] + $value['gugur']}}</td>
                            </tr>
                            @php $no++; $tot_lolos+=$value['lolos']; $tot_gugur+=$value['gugur']; @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2">Total</td>
                        <td>{{ $tot_lolos }}</td>
                        <td>{{ $tot_gugur }}</td>
                        <td>{{ $tot_lolos + $tot_gugur }}</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@section('js')
    <!-- Doughnut Chart -->
    <script>
        $(document).ready(function() {
            var canvasDoughnut,
                options = {
                    legend: {
                        display: true,
                        position: 'bottom',
                    },
                    responsive: true
                };

            new Chart(document.getElementById("canvas1i"), {
                type: 'doughnut',
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                data: {
                    labels: [
                        "Lulus",
                        "Gugur"
                    ],
                    datasets: [{
                        data: [{{ $dash_tot_lolos }}, {{ $dash_tot_gugur }}],
                        backgroundColor: [
                            "#E95E4F",
                            "#9B59B6",
                        ],
                        hoverBackgroundColor: [
                            "#E74C3C",
                            "#B370CF",
                        ]

                    }]
                },
                options: options
            });
        });
    </script>
    <!-- /Doughnut Chart -->
@endsection